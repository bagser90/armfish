package com.example.bagse.armfish.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bagse.armfish.Modeles.Fish;
import com.example.bagse.armfish.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;

public class FishAdapter extends FirestoreRecyclerAdapter<Fish, FishAdapter.FishHolder> {
    private OnItemClickListener listener;
    Context mContext;

    public FishAdapter(@NonNull FirestoreRecyclerOptions<Fish> options, Context context) {
        super(options);
        mContext = context;
    }

    @Override
    protected void onBindViewHolder(@NonNull FishHolder holder, int position, @NonNull Fish model) {
        holder.textViewTitle.setText(model.getTitle());
        Picasso.with(mContext)
                .load(model.getPicUrl())
                .centerCrop()
                .fit()
                .into(holder.imageViewFish);
    }

    @NonNull
    @Override
    public FishHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fish_item, parent, false);
        return new FishHolder(v);
    }

    class FishHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        ImageView imageViewFish;

        public FishHolder(View itemView) {

            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_name);
            imageViewFish = itemView.findViewById(R.id.image_view_upload);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener!=null){
                        listener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
