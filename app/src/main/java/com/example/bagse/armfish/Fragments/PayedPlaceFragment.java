package com.example.bagse.armfish.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bagse.armfish.Adapters.PlaceAdapter;
import com.example.bagse.armfish.Modeles.PlaceModel;
import com.example.bagse.armfish.R;
import com.example.bagse.armfish.UI.PlaceGeoActivity;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayedPlaceFragment extends Fragment {

    public static final String BUNDLE_GEOPOINT = "geo_point";

    private FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    private CollectionReference placeRef = mFirebaseFirestore.collection("FishingPlaces");
    private PlaceAdapter mPlaceAdapter;

    private View rootView;

    public PayedPlaceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_payed_place, container, false);
        initRecyclerView();
        return rootView;
    }

    public void initRecyclerView() {
        Query query = placeRef
                .whereEqualTo("type", 2);
        FirestoreRecyclerOptions<PlaceModel> options = new FirestoreRecyclerOptions.Builder<PlaceModel>()
                .setQuery(query, PlaceModel.class)
                .build();

        mPlaceAdapter = new PlaceAdapter(options, getActivity());

        RecyclerView recyclerView = rootView.findViewById(R.id.recycler_view_vcharovi);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mPlaceAdapter);
        mPlaceAdapter.setOnItemClickListener(new PlaceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                PlaceModel placeModel = documentSnapshot.toObject(PlaceModel.class);


                Intent intent = new Intent(getActivity(), PlaceGeoActivity.class);

                intent.putExtra(BUNDLE_GEOPOINT,placeModel);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        mPlaceAdapter.startListening();
        super.onStart();
    }

    @Override
    public void onStop() {
        mPlaceAdapter.stopListening();
        super.onStop();
    }
}
