package com.example.bagse.armfish.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bagse.armfish.R;
import com.example.bagse.armfish.UI.FishListActivity;
import com.squareup.picasso.Picasso;

public class FishDetailActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView textViewTitle;
    private TextView textViewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_detail);
        getSupportActionBar();


        initViews();

    }

    public void initViews() {
        Intent intent = getIntent();

        String title = intent.getStringExtra(FishListActivity.INTENT_TITLE);
        String description = intent.getStringExtra(FishListActivity.INTENT_DESCRIPTION);
        String picUrl = intent.getStringExtra(FishListActivity.INTENT_PIC_URL);

        imageView=findViewById(R.id.image_view_detail);
        textViewTitle=findViewById(R.id.text_view_title_detail);
        textViewDescription=findViewById(R.id.text_view_description_detail);


        Picasso.with(this).load(picUrl)
                .fit()
                .centerCrop()
                .into(imageView);
        textViewTitle.setText(title);
        textViewDescription.setText(description);
    }
}
