package com.example.bagse.armfish.Modeles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.GeoPoint;

public class PlaceModel implements Parcelable {

    private  GeoPoint geoPoint;
    private String name;
    private String picUrl;
    private String description;
    private String phone;
    private int type;
    private int localeId;


    public PlaceModel() {
    }

    public PlaceModel(String name, String picUrl) {
        this.name = name;
        this.picUrl = picUrl;
    }

    public PlaceModel(GeoPoint geoPoint, String name, String picUrl, String description,String phone ,int type, int localeId) {
        this.geoPoint = geoPoint;
        this.name = name;
        this.picUrl = picUrl;
        this.description = description;
        this.phone=phone;
        this.type = type;
        this.localeId = localeId;
    }

    protected PlaceModel(Parcel in) {
        name = in.readString();
        picUrl = in.readString();
        description=in.readString();
        phone=in.readString();
        type=in.readInt();
        localeId = in.readInt();

        Double lat =in.readDouble();
        Double lng=in.readDouble();
        geoPoint=new GeoPoint(lat,lng);
    }

    public static final Creator<PlaceModel> CREATOR = new Creator<PlaceModel>() {
        @Override
        public PlaceModel createFromParcel(Parcel in) {
            return new PlaceModel(in);
        }

        @Override
        public PlaceModel[] newArray(int size) {
            return new PlaceModel[size];
        }
    };

    public GeoPoint getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(GeoPoint geoPoint) {
        this.geoPoint = geoPoint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getLocaleId() {
        return localeId;
    }

    public void setLocaleId(int localeId) {
        this.localeId = localeId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(picUrl);
        dest.writeString(description);
        dest.writeString(phone);
        dest.writeInt(type);
        dest.writeInt(localeId);
        dest.writeDouble(geoPoint.getLatitude());
        dest.writeDouble(geoPoint.getLongitude());
    }
}
