package com.example.bagse.armfish.Adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bagse.armfish.Modeles.TypeModel;
import com.example.bagse.armfish.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;

public class TypeAdapter extends FirestoreRecyclerAdapter<TypeModel, TypeAdapter.TypeHolder> {
    private OnItemClickListener mListener;


    public TypeAdapter(@NonNull FirestoreRecyclerOptions<TypeModel> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull TypeHolder holder, int position, @NonNull TypeModel model) {
        holder.textViewTitle.setText(position + 1 + ". " + model.getTitle());
    }

    @NonNull
    @Override
    public TypeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.type_item, parent, false);
        return new TypeHolder(v);
    }

    public class TypeHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;

        public TypeHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_type_title);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && mListener!=null){
                        mListener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}
