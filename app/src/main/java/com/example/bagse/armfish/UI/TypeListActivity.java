package com.example.bagse.armfish.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.bagse.armfish.Adapters.TypeAdapter;
import com.example.bagse.armfish.Modeles.TypeModel;
import com.example.bagse.armfish.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class TypeListActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    public static final String INTENT_TITLE = "title";
    public static final String INTENT_DESCRIPTION = "description";
    public static final String INTENT_PIC_URL = "picUrl";

    private FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    private CollectionReference typeRef = mFirebaseFirestore.collection("FishingTypes");
    private TypeAdapter mTypeAdapter1;
    private TypeAdapter mTypeAdapter2;
    private RecyclerView recyclerView1;
    private RecyclerView recyclerView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_list);
        setTitle(R.string.title_type_of_fishing);


        mToolbar = findViewById(R.id.toolbar_list_type);
        setSupportActionBar(mToolbar);


        setUpRecyclerView();
    }


    public void setUpRecyclerView() {
        Query query1 = typeRef.whereEqualTo("type", 1);
        Query query2 = typeRef.whereEqualTo("type", 2);

        FirestoreRecyclerOptions<TypeModel> options1 = new FirestoreRecyclerOptions.Builder<TypeModel>()
                .setQuery(query1, TypeModel.class)
                .build();
        FirestoreRecyclerOptions<TypeModel> options2 = new FirestoreRecyclerOptions.Builder<TypeModel>()
                .setQuery(query2, TypeModel.class)
                .build();

        mTypeAdapter1 = new TypeAdapter(options1);
        mTypeAdapter2 = new TypeAdapter(options2);
        recyclerView1 = findViewById(R.id.recycler_view_gishatich);
        recyclerView2 = findViewById(R.id.recycler_view_voch_gishatich);
        recyclerView1.setHasFixedSize(true);
        recyclerView2.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        recyclerView2.setLayoutManager(new LinearLayoutManager(this));

        recyclerView1.setAdapter(mTypeAdapter1);
        recyclerView2.setAdapter(mTypeAdapter2);


        mTypeAdapter1.setOnItemClickListener(new TypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                TypeModel typeModel = documentSnapshot.toObject(TypeModel.class);
                String title = typeModel.getTitle();
                String description = typeModel.getDescription();
                String picUrl = typeModel.getPicUrl();


                Intent intent = new Intent(getApplicationContext(), TypeDetailActivity.class);
                intent.putExtra(INTENT_TITLE, title);
                intent.putExtra(INTENT_DESCRIPTION, description);
                intent.putExtra(INTENT_PIC_URL, picUrl);
                startActivity(intent);
            }
        });

        mTypeAdapter2.setOnItemClickListener(new TypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                TypeModel typeModel = documentSnapshot.toObject(TypeModel.class);
                String title = typeModel.getTitle();
                String description = typeModel.getDescription();
                String picUrl = typeModel.getPicUrl();


                Intent intent = new Intent(getApplicationContext(), TypeDetailActivity.class);
                intent.putExtra(INTENT_TITLE, title);
                intent.putExtra(INTENT_DESCRIPTION, description);
                intent.putExtra(INTENT_PIC_URL, picUrl);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        mTypeAdapter1.startListening();
        mTypeAdapter2.startListening();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mTypeAdapter1.stopListening();
        mTypeAdapter2.stopListening();
        super.onStop();
    }
}
