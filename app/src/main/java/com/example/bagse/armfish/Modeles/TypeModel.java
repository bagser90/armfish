package com.example.bagse.armfish.Modeles;

public class TypeModel {

    private String title;
    private String description;
    private String picUrl;
    private int localeId;
    private int type;

    public TypeModel() {
    }

    public TypeModel(String title, String description, String picUrl, int localeId, int type) {
        this.title = title;
        this.description = description;
        this.picUrl = picUrl;
        this.localeId = localeId;
        this.type = type;
    }

    public TypeModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public int getLocaleId() {
        return localeId;
    }

    public int getType() {
        return type;
    }
}
