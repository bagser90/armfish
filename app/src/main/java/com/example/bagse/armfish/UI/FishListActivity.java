package com.example.bagse.armfish.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.bagse.armfish.Adapters.FishAdapter;
import com.example.bagse.armfish.Modeles.Fish;
import com.example.bagse.armfish.R;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

public class FishListActivity extends AppCompatActivity {
    public static final String INTENT_TITLE = "title";
    public static final String INTENT_DESCRIPTION = "description";
    public static final String INTENT_PIC_URL = "picUrl";

    private FirebaseFirestore mFirebaseFirestore = FirebaseFirestore.getInstance();
    private CollectionReference fishRef = mFirebaseFirestore.collection("Fishes");
    private FishAdapter mFishAdapter;


    private Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fish_list);
        setTitle(R.string.title_armenian_fishes);

        mToolbar=findViewById(R.id.toolbar_list);
        setSupportActionBar(mToolbar);

        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        Query query = fishRef.orderBy("title", Query.Direction.ASCENDING);
        FirestoreRecyclerOptions<Fish> options = new FirestoreRecyclerOptions.Builder<Fish>()
                .setQuery(query, Fish.class)
                .build();

        mFishAdapter = new FishAdapter(options, this);
        RecyclerView recyclerView = findViewById(R.id.recycler_view_fish);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(mFishAdapter);


        mFishAdapter.setOnItemClickListener(new FishAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(DocumentSnapshot documentSnapshot, int position) {
                Fish fish = documentSnapshot.toObject(Fish.class);
                String title = fish.getTitle();
                String description = fish.getDescription();
                String picUrl = fish.getPicUrl();

                Intent intent = new Intent(getApplicationContext(), FishDetailActivity.class);
                intent.putExtra(INTENT_TITLE, title);
                intent.putExtra(INTENT_DESCRIPTION, description);
                intent.putExtra(INTENT_PIC_URL, picUrl);

                startActivity(intent);
            }
        });
    }

    

    @Override
    protected void onStart() {
        mFishAdapter.startListening();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mFishAdapter.stopListening();
        super.onStop();
    }
}
