package com.example.bagse.armfish.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.bagse.armfish.UI.FishListActivity;
import com.example.bagse.armfish.UI.PlaceListActivity;
import com.example.bagse.armfish.R;
import com.example.bagse.armfish.UI.TypeListActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    private Button mButtonFish;
    private Button mButtonFishingPlaces;
    private Button mButtonKindOfFishing;


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        // Inflate the layout for this fragment


        mButtonFish = v.findViewById(R.id.button_fish);
        mButtonFishingPlaces = v.findViewById(R.id.button_fishing_places);
        mButtonKindOfFishing = v.findViewById(R.id.button_kind_of_fishing);

        mButtonFish.setOnClickListener(this);
        mButtonFishingPlaces.setOnClickListener(this);
        mButtonKindOfFishing.setOnClickListener(this);


        return v;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.button_fish:
                intent = new Intent(getContext(), FishListActivity.class);
                startActivity(intent);
                break;
            case R.id.button_fishing_places:
                intent = new Intent(getContext(), PlaceListActivity.class);
                startActivity(intent);
                break;
            case R.id.button_kind_of_fishing:
                intent = new Intent(getContext(), TypeListActivity.class);
                startActivity(intent);
                break;
        }
    }
}
