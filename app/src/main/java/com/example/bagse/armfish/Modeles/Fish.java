package com.example.bagse.armfish.Modeles;

import android.os.Parcel;
import android.os.Parcelable;

public class Fish implements Parcelable {


    private int localeId;
    private String title;
    private String description;
    private String picUrl;

    public Fish() {
    }

    public Fish(String title, String picUrl) {
        this.title = title;
        this.picUrl = picUrl;
    }

    public Fish(int localeId, String title, String description, String picUrl) {
        this.localeId = localeId;
        this.title = title;
        this.description = description;
        this.picUrl = picUrl;
    }


    protected Fish(Parcel in) {
        localeId = in.readInt();
        title = in.readString();
        description = in.readString();
        picUrl = in.readString();
    }

    public static final Creator<Fish> CREATOR = new Creator<Fish>() {
        @Override
        public Fish createFromParcel(Parcel in) {
            return new Fish(in);
        }

        @Override
        public Fish[] newArray(int size) {
            return new Fish[size];
        }
    };

    public int getLocaleId() {
        return localeId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPicUrl() {
        return picUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(localeId);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(picUrl);
    }
}
