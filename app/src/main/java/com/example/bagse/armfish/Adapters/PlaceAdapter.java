package com.example.bagse.armfish.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bagse.armfish.Modeles.PlaceModel;
import com.example.bagse.armfish.R;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.DocumentSnapshot;
import com.squareup.picasso.Picasso;

public class PlaceAdapter extends FirestoreRecyclerAdapter<PlaceModel, PlaceAdapter.PlaceHolder> {
    private OnItemClickListener mListener;
    Context mContext;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public PlaceAdapter(@NonNull FirestoreRecyclerOptions<PlaceModel> options,Context context) {

        super(options);
        mContext=context;
    }

    @Override
    protected void onBindViewHolder(@NonNull PlaceHolder holder, int position, @NonNull PlaceModel model) {
       holder.textViewName.setText(model.getName());
        Picasso.with(mContext)
                .load(model.getPicUrl())
                .centerCrop()
                .fit()
                .into(holder.imageViewPlace);
    }

    @NonNull
    @Override
    public PlaceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.place_item,parent,false);
        return new PlaceHolder(v);
    }

    public class PlaceHolder extends RecyclerView.ViewHolder{

        TextView textViewName;
        ImageView imageViewPlace;

        public PlaceHolder(View itemView) {
            super(itemView);
            textViewName=itemView.findViewById(R.id.text_view_place_title);
            imageViewPlace=itemView.findViewById(R.id.image_view_place);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && mListener!=null){
                        mListener.onItemClick(getSnapshots().getSnapshot(position),position);
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(DocumentSnapshot documentSnapshot, int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}

