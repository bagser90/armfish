package com.example.bagse.armfish.UI;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bagse.armfish.Fragments.PayedPlaceFragment;
import com.example.bagse.armfish.Modeles.PlaceModel;
import com.example.bagse.armfish.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.GeoPoint;
import com.squareup.picasso.Picasso;

import static com.example.bagse.armfish.Util.Constants.MAPVIEW_BUNDLE_KEY;

public class PlaceGeoActivity extends AppCompatActivity implements OnMapReadyCallback {
    private TextView textViewName;
    private TextView textViewDescription;
    private Button buttonDescription;
    private ImageView mImageView;
    private Toolbar mToolbar;

    private PlaceModel placeModel;

    private MapView mMapView;

    private GoogleMap mGoogleMap;
    private LatLngBounds mMapBoundary;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_geo);
        initViews();

        initGoogleMap(savedInstanceState);


    }


    public void setCameraView() {

        // Քարտեզի շրջակայքի կօօրդինատներ 0,2*0,2=0.04
        double bottomBoundary = placeModel.getGeoPoint().getLatitude();
        double leftBoundary = placeModel.getGeoPoint().getLongitude();
        double topBoundary = placeModel.getGeoPoint().getLatitude() ;
        double rightBoundary = placeModel.getGeoPoint().getLongitude();
        LatLng lato = new LatLng(placeModel.getGeoPoint().getLatitude(), placeModel.getGeoPoint().getLongitude());
        Log.d("aaa", rightBoundary + " " + leftBoundary);

        mMapBoundary = new LatLngBounds(new LatLng(bottomBoundary, leftBoundary)
                , new LatLng(topBoundary, rightBoundary));

        Log.d("aaaa", mMapBoundary.toString());


        mGoogleMap.setLatLngBoundsForCameraTarget(mMapBoundary);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lato, 11));

    }

    public void initGoogleMap(Bundle savedInstanceState) {
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        mMapView = findViewById(R.id.map_view);
        mMapView.onCreate(mapViewBundle);

        mMapView.getMapAsync(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }
        mMapView.onSaveInstanceState(mapViewBundle);
    }

    public void initViews() {
        textViewName = findViewById(R.id.text_view_geo_name);
        textViewDescription = findViewById(R.id.text_view_geo_description);
        buttonDescription = findViewById(R.id.button_geo_description);
        mImageView = findViewById(R.id.image_view_geo);
        mToolbar = findViewById(R.id.toolbar_geo_place);

        setSupportActionBar(mToolbar);
        placeModel = getIntent().getParcelableExtra(PayedPlaceFragment.BUNDLE_GEOPOINT);

        String description = placeModel.getDescription();

        final String phone = placeModel.getPhone();


        String name = placeModel.getName();
        String picUrl = placeModel.getPicUrl();
        GeoPoint geoPoint = placeModel.getGeoPoint();
        double lng = geoPoint.getLongitude();
        double lat = geoPoint.getLatitude();

        Picasso.with(this).load(picUrl)
                .fit()
                .into(mImageView);
        textViewName.setText(name);
        textViewDescription.setText(description);
        buttonDescription.setText(phone);
        buttonDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phone));
                startActivity(intent);
            }
        });

        setTitle(name);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(placeModel.getGeoPoint().getLatitude(), placeModel.getGeoPoint().getLongitude()))
                .title(textViewName.getText().toString()));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        googleMap.setMyLocationEnabled(true);
        mGoogleMap = googleMap;
        setCameraView();
    }


    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
