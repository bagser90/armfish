package com.example.bagse.armfish.UI;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.bagse.armfish.Fragments.FreePlaceFragment;
import com.example.bagse.armfish.Fragments.PayedPlaceFragment;
import com.example.bagse.armfish.R;
import com.example.bagse.armfish.UI.PlaceListActivity;

public class PlaceDetailActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    Fragment mFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        setTitle(R.string.title_place_of_fishing);

        mToolbar = findViewById(R.id.toolbar_list_place);
        setSupportActionBar(mToolbar);

        initFragments();


    }

    public void initFragments() {
        Intent intent = getIntent();
        int fragId = intent.getIntExtra(PlaceListActivity.FRAMENT_ID, 0);

        switch (fragId) {
            case 1:
                mFragment = new FreePlaceFragment();
                break;
            case 2:
                mFragment =new PayedPlaceFragment();
                break;
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_place_frame, mFragment);
        ft.commit();
    }
}
