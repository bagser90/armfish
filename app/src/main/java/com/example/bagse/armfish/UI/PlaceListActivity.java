package com.example.bagse.armfish.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.bagse.armfish.R;

public class PlaceListActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    public static final String FRAMENT_ID="fragmentId";

    private TextView textViewFree;
    private TextView textViewPayed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_list);
        setTitle(R.string.title_place_of_fishing);

        mToolbar = findViewById(R.id.toolbar_list_place);
        setSupportActionBar(mToolbar);

        textViewFree=findViewById(R.id.text_view_anvchar);
        textViewPayed=findViewById(R.id.text_view_vcharovi);

        textViewPayed.setOnClickListener(this);
        textViewFree.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()){
            case R.id.text_view_anvchar:
                intent=new Intent(this,PlaceDetailActivity.class);
                intent.putExtra(FRAMENT_ID,1);
                startActivity(intent);
                break;
            case R.id.text_view_vcharovi:
                intent=new Intent(this,PlaceDetailActivity.class);
                intent.putExtra(FRAMENT_ID,2);
                startActivity(intent);
                break;

        }
    }
}
