package com.example.bagse.armfish.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bagse.armfish.R;
import com.example.bagse.armfish.UI.TypeListActivity;
import com.squareup.picasso.Picasso;

public class TypeDetailActivity extends AppCompatActivity {
    private Toolbar mToolbar;

    private ImageView imageView;
    private TextView textViewTitle;
    private TextView textViewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_detail);

        mToolbar = findViewById(R.id.toolbar_detail_type);
        setSupportActionBar(mToolbar);

        initViews();
    }

    public void initViews(){
        Intent intent = getIntent();

        String title = intent.getStringExtra(TypeListActivity.INTENT_TITLE);
        String description = intent.getStringExtra(TypeListActivity.INTENT_DESCRIPTION);
        String picUrl = intent.getStringExtra(TypeListActivity.INTENT_PIC_URL);

        setTitle(title);

        imageView=findViewById(R.id.image_view_type_detail_image);
        textViewTitle=findViewById(R.id.text_view_type_detail_title);
        textViewDescription=findViewById(R.id.text_view_type_detail_description);

        Picasso.with(this).load(picUrl)
                .fit()
                .into(imageView);
        textViewTitle.setText(title);
        textViewDescription.setText(description);
    }
}
